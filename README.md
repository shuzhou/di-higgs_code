# di-higgs_code
This code is used in di-higgs analysis to improve signal/background ratio.

Two major tools used: gradient BDT (XGBoost) and Neural Network (Keras).

Python version: 3.7 Tensorflow version: 1.13.1 CUDA version: 10.2

Codes are in jupyter notebooks under jupyter folder:
   HH-XGBoost-NN.ipynb: Used to classify signal and background.
   XGBoost-NN*.ipynb:   Used to classify different channel's signals.
   cut.ipynb:           Used to get cut efficiency.
   Apply*ipynb:         Used to apply BDT/NN score to minitrees.
   
BDT models are saved under BDT-model.

Neural Network models are saved under NN-model.
   
In HH-XGBoost-NN.ipynb, GPU accelerated array sum function is used since background array is very large (~GB). If no GPU resource, just delete that part.